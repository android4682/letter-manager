const getParams = new URLSearchParams(window.location.search)
const electron = require('electron')

function findDataFromParent(element, dataName) {
	let snowflake = $(element).data(dataName)
	if (!isEmpty(snowflake)) return snowflake
	else if (element.localName == "body") return null
	do {
		element = element.parentElement
		snowflake = $(element).data(dataName)
		if (!isEmpty(snowflake)) return snowflake
	} while (!isEmpty(element) || element.localName != "body")
	return null
}

/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
function isEmpty(variable) {
	if (variable == null || variable == undefined) return true
	if (typeof variable == "string")
		if (variable == "") return true
	else if (typeof variable == "object") {
		if (Array.isArray(variable))
			if (variable.length == 0) return true
		else if (Object.keys(variable).length == 0) return true
	}
	return false
}

/**
 * My second famous (not really) isEmptyAny function. It checks if any variable in a given array is empty or not
 * @param {Array} array An array with elements you want to know if any of them are empty
 * @returns {Boolean} Returns true on any of them being empty and false on none of them being empty
 */
function isEmptyAny(array) {
	for (let i = 0; i < array.length; i++) {
		if (isEmpty(array[i])) {
			return true
		}
	}
	return false
}

/**
 * Sleep for given amount of milliseconds (sync-able via await)
 * @param {Number} milliSeconds Amount of milliseconds to sleep
 * @returns {Promise} Make sure to await this!
 */
function sleep(milliSeconds) {
	return new Promise(function (resolve, reject) {
		setTimeout(function () {
			resolve()
		}, milliSeconds)
	})
}

const toBase64 = file => new Promise((resolve, reject) => {
	const reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = () => resolve(reader.result);
	reader.onerror = error => reject(error);
});

$("body").ready(function (event) {
	resizeElements()
	$(".copyright #year").text(new Date().getFullYear())
})

function resizeElements() {
	if ($(".navbar").length != 0) $(".sidebar-main").css("min-height", window.innerHeight - parseInt($(".navbar").css("height").replace("px", "")));
}

$(window).resize(resizeElements);

$("a").on("click", function (event) {
	if (
		$(this).hasClass("new-file") ||
		$(this).hasClass("load-file") ||
		$(this).hasClass("clear-recents") ||
		$(this).hasClass("remove-from-recents") ||
		$(this).hasClass("delete-label") ||
		$(this).hasClass("delete-letter") ||
		$(this).hasClass("view-letter") ||
		$(this).hasClass("open-yearbook") ||
		$(this).hasClass("external-window")
	) return true;
	event.preventDefault();
	let href = $(this).attr("href")
	if (href == "#" || href == "#top") return true;
	href = href.replace("-", "_")
	electron.ipcRenderer.send("openPage", href)
	if (window.module) module = window.module;
	return false
})

$("a.open-yearbook").on("click", (event) => electron.ipcRenderer.send("open-yearbook", findDataFromParent(event.currentTarget, "year"), findDataFromParent(event.currentTarget, "args")))

$("a.external-window").on("click", function (event) {
	if ($(this).attr("href") == "#") return true
	event.preventDefault()
	electron.ipcRenderer.send("openExternalLink", $(this).attr("href"))
	return false
})

$("a.clear-recents").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("clearRecents")
	return false
})

$("a.remove-from-recents").on("click", function (event) {
	if (isEmpty($(this).data("location"))) return true
	event.preventDefault()
	electron.ipcRenderer.send("removeFromRecents", $(this).data("location"))
	return false
})

$("a.new-file").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("newBinder", $(this).data("location"))
	return false
})

$("a.load-file").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("loadBinder", $(this).data("location"))
	return false
})

$("a.delete-label").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("deleteLabel", $(this).data("location"))
	return false
})

// TODO: Finish
$("a.view-letter").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("viewLetter", $(this).data("location"))
	return false
})

$("a.delete-letter").on("click", function (event) {
	event.preventDefault()
	electron.ipcRenderer.send("deleteLetter", $(this).data("location"))
	return false
})

electron.ipcRenderer.on('task-success', (event, msg) => {
	if (isEmpty(msg)) location.replace(location.pathname + "?task=success")
	else location.replace(location.pathname + "?task=success&msg=" + encodeURIComponent(msg))
})

electron.ipcRenderer.on('task-failed', (event, msg) => {
	if (isEmpty(msg)) location.replace(location.pathname + "?task=failed")
	else location.replace(location.pathname + "?task=failed&msg=" + encodeURIComponent(msg))
})

var getValueRes = undefined
async function getValueFromBackEnd(event, args = []) {
	getValueRes = undefined
	args.unshift(event)
	args.push("getValueFromBackEnd")
	electron.ipcRenderer.send.apply(this, args)
	let skip = false
	let timeout = setTimeout(() => skip = true, 10000)
	while (getValueRes == undefined && !skip) await sleep(100)
	if (skip) return undefined
	else {
		clearTimeout(timeout)
		let res = getValueRes
		getValueRes = undefined
		return res
	}
}

electron.ipcRenderer.on('getValueFromBackEnd', (event, res) => getValueRes = res.msg)

if (getParams.has("task")) {
	let taskStatus = getParams.get("task")
	let msg
	if (getParams.has("msg")) msg = getParams.get("msg")
	console.log("Got message")
	console.dir(msg)
	console.dir(window.location.search)
	if (isEmpty(msg) || msg === "[object Object]") {
		if (taskStatus == "success") toastr.success(undefined, "Task completed successfully!")
		else if (taskStatus == "failed") toastr.error(undefined, "Task failed!")
	} else {
		if (taskStatus == "success") toastr.success(msg, "Task completed successfully!")
		else if (taskStatus == "failed") toastr.error(msg, "Task failed!")
	}
}