// Setting prototypes - tmp.gpro
// WARNING: This breaks electron for some reason..
/* Object.prototype.size = function() {
    let size = 0, key
    for (key in this) {
        if (this.hasOwnProperty(key)) size++
    }
    return size
} */

// Name of the script
const programName = "Letter Manager";
// Setting module name which will be used by debug handler
const globalModuleName = "letter-manager";
const package = require("./package.json")
const version = "v" + package.version
var moduleName = globalModuleName;

// Global variables


// Loading Droid modules and initializing them
const {
  DroidsError
} = require("droid-errors");

// Error Classes - tmp.derrors
class ImpossibleState extends DroidsError {
  /**
   * Constructing ImpossibleState
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   * @param {String} errMsg **The error message*
   */
  constructor(moduleName, cause, errMsg) {
    super("NOBODY EXPECTS THE SPANISH INQUISITION!\n" + errMsg, moduleName, cause);
  }
}

class MissingFile extends DroidsError {
  /**
   * Constructing MissingFile
   * @param {String} errMsg **The error message*
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   */
  constructor(fileLoc, moduleName, cause) {
    super(`File '${fileLoc}' couldn't be found!`, moduleName, cause)
  }
}

class MissingArgument extends DroidsError {
  /**
   * Constructing SecurityError
   * @param {String} errMsg **The error message*
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   */
  constructor(missingVarName, moduleName, cause) {
    super("Missing argument '" + missingVarName + "'", moduleName, cause)
  }
}

class IncorrectArgument extends DroidsError {
  /**
   * Constructing SecurityError
   * @param {String} errMsg **The error message*
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   */
  constructor(incorrectVarName, descriptionRightUse, moduleName, cause) {
    super(`Argument '${incorrectVarName}' is used incorrectly. Usage:\n${descriptionRightUse}`, moduleName, cause)
  }
}

class Exists extends DroidsError {
  /**
   * Constructing Exists
   * @param {String} errMsgOverride **The error message*
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   */
  constructor(existingVariable, moduleName, cause, errMsgOverride) {
    super((isEmpty(errMsgOverride)) ? `This '${existingVariable}' already exists.` : errMsgOverride, moduleName, cause)
  }
}

class NotExists extends DroidsError {
  /**
   * Constructing Exists
   * @param {String} errMsgOverride **The error message*
   * @param {String} moduleName **Custom module name*
   * @param {Error} cause **If re-throwing pass the original cause here*
   */
  constructor(existingVariable, moduleName, cause, errMsgOverride) {
    super((isEmpty(errMsgOverride)) ? `This '${existingVariable}' does NOT exists.` : errMsgOverride, moduleName, cause)
  }
}

// Setting Debug
const DroidsDebug = require("droid-debug");
// Enabling debug mode
var debugHand = new DroidsDebug(0);
if (process.argv.indexOf("debug") != -1) {
  debugHand = new DroidsDebug(1);
} else if (process.argv.indexOf("killme") != -1) {
  debugHand = new DroidsDebug(2);
} else if (process.argv.indexOf("devops") != -1) {
  debugHand = new DroidsDebug(3);
}

// If an error occurs through debug handler print a message after the error to show that the program is still alive
debugHand.setPostErrorMsgSwitch(true);

debugHand.log("__dirname: " + __dirname, 1, moduleName)

// Flags - tmp.flags


// Loading modules - tmp.modules
// Load electron module
const electron = require("electron");
debugHand.log("Module 'electron' loaded as 'electron'.", 1, moduleName);
// Load ejs-electron module
const ejse = require("ejs-electron");
debugHand.log("Module 'ejs-electron' loaded as 'ejse'.", 1, moduleName);
// Load fs module
const fs = require("fs");
debugHand.log("Module 'fs' loaded as 'fs'.", 1, moduleName);
// Load os module
const os = require("os");
debugHand.log("Module 'os' loaded as 'os'.", 1, moduleName);
// Load crypto module
const crypto = require("crypto");
debugHand.log("Module 'crypto' loaded as 'crypto'.", 1, moduleName);
// Load util module
const util = require("util");
debugHand.log("Module 'util' loaded as 'util'.", 1, moduleName);
// Load sqlite3 module
const sqlite3 = require("sqlite3");
const { shell } = require("electron");
debugHand.log("Module 'sqlite3' loaded as 'sqlite3'.", 1, moduleName)

// Prototypes of loaded modules - tmp.proto.modules
touch = function (path) {
  return (!fs.existsSync(path)) ? fs.writeFileSync(path, "") : null;
}

sqlite3.Database.prototype.runSync = (sql, param) => {
  let db = this
  return new Promise((resolve, reject) => {
    db.run(sql, param, (err) => {
      if (err) reject(err)
      else resolve()
    })
  })
}

sqlite3.Database.prototype.getSync = (sql, param) => {
  let db = this
  return new Promise((resolve, reject) => {
    db.get(sql, param, (err, row) => {
      if (err) reject(err)
      else resolve(row)
    })
  })
}

sqlite3.Database.prototype.allSync = (sql, param) => {
  let db = this
  return new Promise((resolve, reject) => {
    db.all(sql, param, (err, rows) => {
      if (err) reject(err)
      else resolve(rows)
    })
  })
}

// Classes
const sqlite3Sync = {
  "Database": class Database {
    constructor(fileLocation) {
      this.loaded = false
      this.fckTheConstructor(fileLocation)
    }

    async fckTheConstructor(fileLocation) {
      let moduleName = "sqlite3Sync.Database"
      if (isEmpty(fileLocation)) throw new MissingArgument("fileLocation", moduleName)
      let dbRes = null
      this.db = new sqlite3.Database(fileLocation, (err) => {
        if (err) dbRes = err
        else dbRes = true
      })
      while (dbRes === null) await sleep(1)
      if (dbRes !== true) throw dbRes
      this.loaded = true
    }

    isLoaded() {
      return this.loaded
    }
    async run(sql, params = []) {
      let res = null
      this.db.run(sql, params, (err) => {
        if (err) res = err
        else res = true
      })
      while (res === null) await sleep(1)
      if (res !== true) throw res
      return res
    }

    async get(sql, params = []) {
      let res = null
      let success = null
      this.db.get(sql, params, (err, row) => {
        if (err) {
          res = err
          success = false
        } else {
          res = row
          success = true
        }
      })
      while (success === null) await sleep(1)
      if (success) return res
      else throw res
    }

    async all(sql, params = []) {
      let res = null
      let success = null
      this.db.all(sql, params, (err, rows) => {
        if (err) {
          res = err
          success = false
        } else {
          res = rows
          success = true
        }
      })
      while (success === null) await sleep(1)
      if (success) return res
      else throw res
    }
  }
}

class electronLogHandling {
  static error(title, msg, moduleName, dirVar) {
    debugHand.error(msg, moduleName, dirVar);
    if (isEmpty(title)) title = `An error has occurred with ${moduleName}`;
    if (debugHand.getDebugMode() != "NO-DEBUG") dialog.showErrorBox(title, `${(!isEmpty(msg)) ? `${msg}\n` : ""}\n${moduleName}'s stack:\n${(!isEmpty(dirVar)) ? util.inspect(dirVar, false, 1, false) : "missing"}`);
    else dialog.showErrorBox(title, (!isEmpty(msg)) ? msg : null);
  }
}

class RecentFilesManager {

  constructor(dataSaveLocation) {
    let moduleName = "RecentFilesManager [constructor]";
    if (isEmpty(dataSaveLocation)) throw new MissingArgument("dataSaveLocation", moduleName);
    this.fileLoc = dataSaveLocation;
    this.saving = false;
    if (!fs.existsSync(this.fileLoc)) this.create(this.fileLoc)
    else this.load();
  }

  create(dataSaveLocation) {
    touch(dataSaveLocation);
    this.data = [];
    this.save();
  }

  load() {
    let moduleName = "RecentFilesManager.load()";
    if (!fs.existsSync(this.fileLoc)) throw new MissingFile(this.fileLoc, moduleName);
    this.data = JSON.parse(fs.readFileSync(this.fileLoc));
    return true;
  }

  save() {
    let moduleName = "RecentFilesManager.save()";
    if (!this.saving) {
      this.saving = true;
      if (!fs.existsSync(this.fileLoc)) {
        this.saving = false;
        throw new MissingFile(this.fileLoc, moduleName);
      }
      fs.writeFileSync(this.fileLoc, JSON.stringify(this.data));
      this.saving = false;
      debugHand.log("Saving completed!", 3, moduleName);
      return true;
    } else {
      let dataBin = this;
      debugHand.log("Saving is already taking place. Waiting until other request is done...", 1, moduleName);
      setTimeout(function () {
        dataBin.save();
      }, 100);
    }
  }

  getFileLocation() {
    return this.fileLoc
  }

  getData() {
    return this.data;
  }

  setData(data) {
    this.data = data;
    return this.save();
  }

  // Custom Functions
  sortData(sortingKey = "timestamp", way = "desc") {
    let moduleName = "RecentFilesManager.sortDataToRecentTimestamps()";
    if (way == "asc") this.data.sort((a, b) => a[sortingKey] - b[sortingKey])
    else if (way == "desc") this.data.sort((a, b) => b[sortingKey] - a[sortingKey])
    return true
  }

  generateRecentList(limit = 10) {
    let moduleName = "RecentFilesManager.generateRecentList()";
    if (isEmpty(limit)) throw new MissingArgument("limit", moduleName)
    if (limit > this.data.length) limit = this.data.length
    // Actual execution code
    this.sortData()
    let htmlString = ``
    for (let i = 0; i < limit; i++) {
      const recentBinder = this.data[i];
      htmlString += `<tr>
    <td><a href="#" class="load-file" data-location="${recentBinder.location}">${recentBinder.name}</a></td>
    <td>${recentBinder.location}</td>
    <td>${new Date(recentBinder.timestamp).toLocaleString()}</td>
    <td>
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="flaticon-more-button-of-three-dots"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item remove-from-recents" data-location="${recentBinder.location}" href="#"><i class="fas fa-times text-orange-red"></i>Remove from list</a>
            </div>
        </div>
    </td>
</tr>`
    }
    return htmlString
  }

  getRecentIndex(searchField, searchValue) {
    let moduleName = "RecentFilesManager.getRecent()";
    if (isEmpty(searchField)) throw new MissingArgument("searchField", moduleName);
    if (isEmpty(searchValue)) throw new MissingArgument("searchValue", moduleName);
    // Actual execution code
    for (let i = 0; i < this.data.length; i++) {
      const recentBinder = this.data[i];
      if (recentBinder[searchField] == searchValue) return i;
    }
    return -1;
  }

  isRecent(fileLocation) {
    let moduleName = "RecentFilesManager.isRecent()";
    if (isEmpty(fileLocation)) throw new MissingArgument("fileLocation", moduleName);
    // Actual execution code
    for (let i = 0; i < this.data.length; i++) {
      const recentBinder = this.data[i];
      if (recentBinder.location == fileLocation) return true;
    }
    return false;
  }

  addRecent(fileName, fileLocation, timestamp = Date.now(), saveAfterwards = true) {
    let moduleName = "RecentFilesManager.addRecent()";
    if (isEmpty(fileName)) throw new MissingArgument("fileName", moduleName);
    if (isEmpty(fileLocation)) throw new MissingArgument("fileLocation", moduleName);
    if (isEmpty(timestamp)) throw new MissingArgument("timestamp", moduleName);
    // Actual execution code
    if (this.isRecent(fileLocation)) this.updateRecent(fileLocation, timestamp, false);
    else this.data.push({
      name: fileName,
      location: fileLocation,
      timestamp
    });
    if (saveAfterwards) this.save();
    return true;
  }

  updateRecent(fileLocation, timestamp = Date.now(), saveAfterwards = true) {
    let moduleName = "RecentFilesManager.updateRecent()";
    if (isEmpty(fileLocation)) throw new MissingArgument("fileLocation", moduleName);
    if (isEmpty(timestamp)) throw new MissingArgument("timestamp", moduleName);
    // Actual execution code
    let ri = this.getRecentIndex("location", fileLocation);
    if (ri != -1) {
      this.data[ri].timestamp = timestamp;
      if (saveAfterwards) this.save();
      return true;
    } else return false;
  }

  removeRecent(by, value) {
    let moduleName = "RecentFilesManager.removeRecent()";
    if (isEmpty(by)) throw new MissingArgument("by", moduleName);
    if (isEmpty(value)) throw new MissingArgument("value", moduleName);
    // Actual execution code
    let ri = this.getRecentIndex(by, value);
    if (ri != -1) this.data.splice(ri, 1);
    return true;
  }
}

/**
 * 	Data structure:
 * 	{
 * 		"letters": [
 * 			{
 * 				uid: // Unique ID
 * 				from: // Company or person
 * 				to: // Company or person
 * 				date: // Date Timestamp
 * 				year: // Year number from timestamp
 * 				labels: [ // Label UIDs ],
 * 				content: // Content converted to base64
 * 				type: // Example PDF: data:application/pdf;base64,
 * 			}, // Next letter
 * 		],
 * 		"labels": [
 * 			{
 * 				uid: // Unique ID
 * 				name: // Name
 * 				color: // Hexcode, Full HTML RGB code, Full HTML HSL code, HTML color name
 * 			}, // Next label
 * 		],
 * 		"settings": [
 * 			your_name: // Your name to be defaulted to when adding letters
 * 		]
 * 	}
 */
class DataManager {

  constructor(dataSaveLocation, action) {
    let moduleName = "DataManager [constructor]";
    if (isEmpty(dataSaveLocation)) throw new MissingArgument("dataSaveLocation", moduleName);
    this.fileLoc = dataSaveLocation;
    this.saving = false;
    this.fileName = dataSaveLocation.match(/\\/) ? dataSaveLocation.split("\\")[dataSaveLocation.split("\\").length - 1].replace(".bndr", "") : dataSaveLocation.match(/\//) ? dataSaveLocation.split("/")[dataSaveLocation.split("/").length - 1].replace(".bndr", "") : null
    switch (action) {
      case "load":
        if (fs.existsSync(this.fileLoc)) this.load();
        else throw new MissingFile(dataSaveLocation, moduleName);
        break;
      case "create":
        this.create(this.fileLoc).then(() => debugHand.log("Database created!", 1, moduleName)).catch((e) => {
          electronLogHandling.error("Data manager error", "An error has ocurred while creating the database.", moduleName, e)
          fs.unlinkSync(dataSaveLocation)
        })
        break;
      default:
        break;
    }
  }

  async create(dataSaveLocation) {
    let moduleName = "DataManager.create()";
    dataSaveLocation = dataSaveLocation || this.fileLoc
    if (isEmpty(dataSaveLocation)) throw new MissingArgument("dataSaveLocation", moduleName)
    this.data = new sqlite3Sync.Database(this.fileLoc)
    while (this.data.isLoaded() === false) await sleep(1)
    try {
      await this.data.run("CREATE TABLE `letters` ( `uid` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `from` TEXT NOT NULL, `to` TEXT NOT NULL, `date` INTEGER NOT NULL, `year` INTEGER NOT NULL, `labels` TEXT NOT NULL DEFAULT '[]', `subject` TEXT NULL DEFAULT NULL, `content` TEXT NOT NULL, `type` TEXT NOT NULL )", [])
      await this.data.run("CREATE TABLE `labels` ( `uid` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `color` TEXT NOT NULL DEFAULT '#FFF' )", [])
      await this.data.run("CREATE TABLE `settings` ( `setting` TEXT NOT NULL PRIMARY KEY, `value` TEXT DEFAULT NULL)", [])
      await this.data.run("INSERT INTO `settings`(`setting`, `value`) VALUES ('name', '')", [])
    } catch (e) {
      electronLogHandling.error("Data manager error", "An error has ocurred while creating the database.", moduleName, e)
      fs.unlinkSync(dataSaveLocation)
    }
  }

  async load() {
    let moduleName = "DataManager.load()";
    if (!fs.existsSync(this.fileLoc)) throw new MissingFile(this.fileLoc, moduleName);
    this.data = new sqlite3Sync.Database(this.fileLoc)
    while (this.data.isLoaded() === false) await sleep(1)
    this.data.run("CREATE TABLE IF NOT EXISTS `letters` ( `uid` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `from` TEXT NOT NULL, `to` TEXT NOT NULL, `date` INTEGER NOT NULL, `year` INTEGER NOT NULL, `labels` TEXT NOT NULL DEFAULT '[]', `subject` TEXT NULL DEFAULT NULL, `content` TEXT NOT NULL, `type` TEXT NOT NULL )")
    await sleep(1)
    this.data.run("CREATE TABLE IF NOT EXISTS `labels` ( `uid` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `color` TEXT NOT NULL DEFAULT '#FFF' )")
    await sleep(1)
    this.data.run("CREATE TABLE IF NOT EXISTS `settings` ( `setting` TEXT NOT NULL PRIMARY KEY, `value` TEXT DEFAULT NULL)")
    await sleep(1)
    this.data.run("INSERT OR IGNORE INTO `settings`(`setting`) VALUES ('name')")
    await sleep(1)
    return true;
  }

  getFileLocation() {
    return this.fileLoc
  }

  getFileName() {
    return this.fileName
  }

  // Tool API
  async isTable(tableName) {
    let moduleName = "DataManager.isTable()";
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    try {
      await this.data.get("SELECT 1 FROM `?`", [tableName])
    } catch (e) {
      return false
    }
    return true
  }

  async isUID(uid, subject) {
    let moduleName = "DataManager.isUID()";
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    if (isEmpty(subject)) throw new MissingArgument("subject", moduleName)
    if (!await this.isTable(subject)) throw new IncorrectArgument("subject", "Must be a valid subject in the data bin structure.", moduleName)
    // Actual execution code
    return (!isEmpty(await this.data.all("SELECT `uid` FROM `?` WHERE `uid`=?", [subject, uid])))
  }

  // Custom Functions
  async getLetter(uid) {
    let moduleName = "DataManager.getLetter()"
    let letter = await this.data.all("SELECT * FROM `letters` WHERE `uid`=?LIMIT 1", [uid])
    return letter[0]
  }

  async getLetters(year) {
    let moduleName = "DataManager.getLetters()"
    return (isEmpty(year)) ? await this.data.all("SELECT * FROM `letters`") : await this.data.all("SELECT * FROM `letters` WHERE `year`=?", [year])
  }

  async getLetterEJS(uid) {
    let moduleName = "DataManager.getLetterEJS()";
    // Actual execution code
    let htmlString = ``
    let letter = await this.getLetter(uid)
    ejse.data("__uid", letter.uid);
    ejse.data("__from", letter.from);
    ejse.data("__to", letter.to);
    ejse.data("__date", letter.date);
    ejse.data("__year", letter.year);
    let labels = JSON.parse(letter.labels)
    let labelsHTML = await this.getLabelHTMLOfLetter(undefined, letter.uid)
    if (labelsHTML == "<i>No labels</i>") labelsHTML = `<span style="font-style:italic;font-weight:normal;">No labels set</span>`
    ejse.data("__labels", labelsHTML);
    ejse.data("__subject", letter.subject);
    ejse.data("__content", letter.content);
    ejse.data("__type", letter.type);
    return htmlString
  }

  async getLabelHTMLOfLetter(labelArr, letterUID) {
    let moduleName = "DataManager.getLabelHTMLOfLetter()";
    if (!isEmpty(letterUID)) {
      labelArr = await this.getLetter(letterUID)
      if (labelArr.labels == "[]") return "<i>No labels</i>"
      else labelArr = JSON.parse(labelArr.labels)
    } else if (isEmpty(labelArr)) throw new MissingArgument("labelArr|letterUID")
    let labelsHTML = ""
    try {
      let tmpLabels = (typeof labelArr == "string") ? JSON.parse(labelArr) : labelArr
      for (let i = 0; i < tmpLabels.length; i++) {
        const labelID = tmpLabels[i];
        let label = await this.getLabel(labelID)
        if (i > 0) labelsHTML += `, <b style="color:${label.color};">${label.name}</b>`
        else labelsHTML += `<b style="color:${label.color};">${label.name}</b>`
      }
    } catch (e) {
      labelsHTML = `<i style="color:red;">Error</i>`
    }
    return labelsHTML
  }

  async generateLettersList(year) {
    let moduleName = "DataManager.generateLettersList()";
    // Actual execution code
    let htmlString = ``
    let letters = await this.getLetters(year)
    for (let i = 0; i < letters.length; i++) {
      const letter = letters[i];
      let labelsHTML = ""
      if (letter.labels == "[]") labelsHTML = "<i>No labels</i>"
      else labelsHTML = await this.getLabelHTMLOfLetter(JSON.parse(letter.labels))
      htmlString += `<tr>
    <td>
        <div class="form-check">
            <input type="checkbox" class="form-check-input">
            <label class="form-check-label">#${i+1}</label>
        </div>
    </td>
    <td>${letter.from}</td>
    <td>${letter.to}</td>
    <td>${letter.subject}</td>
    <td>${new Date(letter.date).toLocaleString()}</td>
    <td>${labelsHTML}</td>
    <td>
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="flaticon-more-button-of-three-dots"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item view-letter" data-location="${letter.uid}" href="#"><i class="fas fa-eye text-green"></i>View</a>
                <a class="dropdown-item delete-letter" data-location="${letter.uid}" href="#"><i class="fas fa-trash-alt text-orange-red"></i>Delete</a>
            </div>
        </div>
    </td>
</tr>`
    }
    return htmlString
  }

  async addLetter(from, to = os.userInfo().username, subject, date = Date.now(), labels = '[]', content, type) {
    let moduleName = "DataManager.addLetter()";
    if (isEmpty(from)) throw new MissingArgument("from", moduleName)
    if (isEmpty(to)) to = os.userInfo().username
    if (isEmpty(date)) date = Date.now()
    if (!labels) labels = '[]'
    if (typeof labels === "object") labels = JSON.stringify(labels)
    if (isEmpty(content)) throw new MissingArgument("content", moduleName)
    if (isEmpty(type)) throw new MissingArgument("type", moduleName)
    // Actual execution code
    await this.data.run("INSERT INTO `letters`(`from`, `to`, `date`, `year`, `labels`, `subject`, `content`, `type`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [from, to, date, new Date(date).getFullYear(), labels, subject, content, type])
    return await this.data.get("SELECT * FROM `letters` ORDER BY `uid` DESC LIMIT 1")
  }

  async editLetter(uid, field, newValue) {
    let moduleName = "DataManager.editLetter()"
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    if (isEmpty(field)) throw new MissingArgument("field", moduleName)
    if (isEmpty(newValue) && newValue !== null) newValue = null
    // Actual execution code
    let res = await this.data.run("UPDATE `letters` SET `" + field + "`=? WHERE `uid`=?", [newValue, uid])
    if (res !== true) throw new DroidsError(res, moduleName)
    return res
  }

  async deleteLetter(uid) {
    let moduleName = "DataManager.deleteLetter()";
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    // Actual execution code
    return await this.data.run("DELETE FROM `letters` WHERE `uid`=?", [uid])
  }

  async getLabels() {
    return await this.data.all("SELECT * FROM `labels`")
  }

  async getLabel(uid) {
    let moduleName = "DataManager.getLabel()"
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    // TODO: Difference between all and get?
    let res = await this.data.all("SELECT * FROM `labels` WHERE `uid`=? LIMIT 1", [uid])
    return (res.length > 0) ? res[0] : null
  }

  async getLabelsName(name) {
    let moduleName = "DataManager.getLabelsName()"
    if (isEmpty(name)) throw new MissingArgument("name", moduleName)
    return await this.data.all("SELECT * FROM `labels` WHERE `name`=?", [name])
  }

  async generateLabelList() {
    let moduleName = "DataManager.generateLabelList()";
    // Actual execution code
    let htmlString = ``
    let labels = await this.getLabels()
    for (let i = 0; i < labels.length; i++) {
      const label = labels[i];
      htmlString += `<tr>
    <td>
        <div class="form-check">
            <input type="checkbox" class="form-check-input">
            <label class="form-check-label">#${i+1}</label>
        </div>
    </td>
    <td>${label.name}</td>
    <td style="color:${label.color};">${label.color}</td>
    <!-- TODO: Maybe show amount of letters that use this label? -->
    <td>
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="flaticon-more-button-of-three-dots"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item delete-label" data-location="${label.uid}" href="#"><i class="fas fa-trash-alt text-orange-red"></i>Delete</a>
            </div>
        </div>
    </td>
</tr>`
    }
    return htmlString
  }

  async generateLabelCheckboxes(letterUID) {
    let moduleName = "DataManager.generateLabelCheckboxes()";
    // Actual execution code
    let htmlString = ``
    let labels = await this.getLabels()
    let letterLabels = []
    if (!isEmpty(letterUID)) {
      letterLabels = await this.data.get("SELECT `labels` FROM `letters` WHERE `uid`=?", [letterUID])
      letterLabels = JSON.parse(letterLabels.labels)
    }
    if (labels.length == 0) return `<label style="font-style:italic;">No labels available..</label>`
    let j = 0
    for (let i = 0; i < labels.length; i++) {
      const label = labels[i];
      if (j == 0) htmlString += `<div style="width:100%;display:inline-flex;">`
      htmlString += `<div style="display:inline-flex;align-items:baseline;margin-right:15px;"><input type="checkbox" class="label" id="label-${i+1}" name="label-${i+1}" value="${label.uid}" ${(letterLabels.indexOf(label.uid) != -1) ? "checked" : ""}><label for="label-${i+1}" style="color:${label.color};margin-left:10px;">${label.name}</label></div>`
      j++
      if (j == 3) {
        j = 0
        htmlString += `</div>`
      }
    }
    if (j != 0) htmlString += `</div>`
    return htmlString
  }

  async addLabel(name, color = "#ff0000") {
    let moduleName = "DataManager.addLabel()";
    if (isEmpty(name)) throw new MissingArgument("name", moduleName)
    if (!color) color = "#ff0000"
    let exists = await this.data.get("SELECT * FROM `labels` WHERE `name`=? LIMIT 1", [name])
    if (!isEmpty(exists)) throw new Exists("name", moduleName)
    // Actual execution code
    await this.data.run("INSERT INTO `labels`(`name`, `color`) VALUES (?, ?)", [name, color])
    return await this.data.get("SELECT * FROM `labels` ORDER BY `uid` DESC LIMIT 1")
  }

  async deleteLabel(uid) {
    let moduleName = "DataManager.deleteLabel()"
    if (isEmpty(uid)) throw new MissingArgument("uid", moduleName)
    // Actual execution code
    return await this.data.run("DELETE FROM `labels` WHERE `uid`=?", [uid])
  }

  async getSetting(setting) {
    let moduleName = "DataManager.getSetting()"
    if (isEmpty(setting)) throw new MissingArgument("setting", moduleName)
    // Actual execution code
    let res = await this.data.get("SELECT `value` FROM `settings` WHERE `setting`=? LIMIT 1", [setting])
    return res.value
  }

  async editSetting(setting, value) {
    let moduleName = "DataManager.editSetting()"
    if (isEmpty(setting)) throw new MissingArgument("setting", moduleName)
    // Actual execution code
    return await this.data.run("UPDATE `settings` SET `value` = ? WHERE `setting`=?", [value, setting])
  }

}

// Loading config variables
if (!fs.existsSync(getConfigFolder())) {
  fs.mkdirSync(getConfigFolder(), {
    recursive: true
  });
}
var recentBinders
if (fs.existsSync(getConfigFolder() + "/recent_binders.json")) {
  try {
    recentBinders = new RecentFilesManager(getConfigFolder() + "/recent_binders.json");
    debugHand.log("Loaded the 'recent binders' file", 1, moduleName);
  } catch (e) {
    if (e instanceof DroidsError) electronLogHandling.error(null, e.errMsg, e.moduleName, e.stack);
    else electronLogHandling.error(null, e.message, moduleName, e.stack);
  }
}
var binder;

// General functions - tmp.gfunc
/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
function isEmpty(variable) {
  if (variable == null || variable == undefined) return true
  if (typeof variable == "string") {
    if (variable == "") return true
  } else if (typeof variable == "object") {
    if (Array.isArray(variable)) {
      if (variable.length == 0) return true
    } else {
      if (Object.keys(variable).length == 0) return true
    }
  }
  return false
}

function factorial(n) {
  var result = n;
  if (n < 0) return null;
  if (n === 1 || n === 0) return 1;
  else {
    while (n >= 2) {
      result = result * (n - 1);
      n--;
    }
    return result;
  }
}

/**
 * Sleep for given amount of milliseconds (sync-able via await)
 * @param {Number} milliSeconds Amount of milliseconds to sleep
 * @returns {Promise} Make sure to await this!
 */
function sleep(milliSeconds) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve()
    }, milliSeconds)
  })
}

function randomStringGenerator(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function getConfigFolder() {
  let moduleName = "getConfigFolder()";
  if (os.type() == "Linux") return os.homedir() + "/.config/" + globalModuleName;
  else if (os.type() == "Windows_NT") return os.homedir() + "\\AppData\\Local\\" + globalModuleName;
  else if (os.type() == "Darwin") return os.homedir() + "/.config/" + globalModuleName; // TODO: Check where Mac's user config folder is
  else throw new ImpossibleState(moduleName, null, "OS must be Windows, Linux or MacOS.");
}


// Program's helper functions
async function menuYearSubBindersGenerator() {
  let html = ""
  try {
    let rows = await binder.data.all("SELECT DISTINCT `year` FROM `letters`")
    for (let i = 0; i < rows.length; i++) html += `<li class="nav-item" data-year="${rows[i].year}" data-args="[]"><a href="#" class="nav-link open-yearbook"><i class="fas fa-book"></i><span>${rows[i].year}</span></a></li>`
    return html || `<li class="nav-item">
    <a href="#" class="nav-link"><span style="font-style:italic;">No year binders yet...<br>Start adding letters!</span></a>
</li>`
  } catch (e) {
    return `<li class="nav-item">
    <a href="#" class="nav-link"><span style="font-style:italic;">Error getting binders<br>Don't worry you can find them in letter overview</span></a>
</li>`
  }
}

async function menuGenerator() {
  let moduleName = "menuGenerator()";
  let menuItems = [
    /* {
        href: "dashboard.ejs",
        name: "Dashboard",
        icon: "fas fa-tachometer-alt"
    }, */
    {
      href: "letters_overview.ejs",
      name: "Letters Overview",
      icon: "fas fa-search"
    },
    {
      role: "spacer"
    },
    {
      role: "year_binders"
    },
    {
      role: "spacer"
    },
    {
      href: "manage_labels.ejs",
      name: "Manage Labels",
      icon: "fas fa-tags"
    },
    {
      href: "settings.ejs",
      name: "Settings",
      icon: "fas fa-cogs"
    }
  ];
  let retVal = `<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color">
    <div class="mobile-sidebar-header d-md-none">
         <div class="header-logo">
             <a href="index.html"><img src="../img/logo/logo1.png" alt="logo"></a>
         </div>
    </div>
    <div class="sidebar-menu-content">
    <ul class="nav nav-sidebar-menu sidebar-toggle-view">`;
  for (let i = 0; i < menuItems.length; i++) {
    const menuItem = menuItems[i];
    if (menuItem.role == "year_binders") retVal += await menuYearSubBindersGenerator();
    else if (menuItem.role == "spacer") retVal += `<hr>`;
    else if (isEmpty(menuItem.role)) retVal += `<li class="nav-item">
    <a href="${menuItem.href}" class="nav-link"><i class="${menuItem.icon}"></i><span>${menuItem.name}</span></a>
</li>`;
  }
  retVal += `	</ul>
    </div>
</div>`
  return retVal;
}


// Program specific variables
const app = electron.app;
const ipcMain = electron.ipcMain;
const dialog = electron.dialog;
var icon = `${__dirname}/assets/img/logo/${(process.platform == "win32") ? "favicon_512.ico" : "favicon.png"}`
var mainWindow;


// Program's (handler) functions
// Page handling
function index_handling() {
  let moduleName = "Electron/index_handling()";
  ejse.data("programName", programName);
  if (!fs.existsSync(getConfigFolder() + "/recent_binders.json")) return firstTime_handling()
  else return welcomeRecent_handling()
}

function firstTime_handling() {
  let moduleName = "Electron/firstTime_handling()";
  ejse.data("programName", programName);
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/first_time.ejs`);
}

function welcomeRecent_handling() {
  let moduleName = "Electron/welcomeRecent_handling()";
  ejse.data("programName", programName);
  ejse.data("recentList", recentBinders.generateRecentList())
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/welcome-recents.ejs`);
}

async function dashboard_handling() {
  let moduleName = "Electron/dashboard_handling()";
  ejse.data("programName", programName);
  ejse.data("menuHTML", await menuGenerator());
  ejse.data("fileName", binder.getFileName());
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/dashboard.ejs`);
}

async function letters_overview_handling() {
  let moduleName = "Electron/letters_overview_handling()";
  ejse.data("programName", programName);
  ejse.data("fileName", binder.getFileName());
  ejse.data("menuHTML", await menuGenerator());
  ejse.data("letterList", await binder.generateLettersList());
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/letters_overview.ejs`);
}

async function add_letter_handling() {
  let moduleName = "Electron/add_letter_handling()";
  ejse.data("programName", programName);
  ejse.data("fileName", binder.getFileName());
  ejse.data("checkboxLabelsHTML", await binder.generateLabelCheckboxes());
  ejse.data("menuHTML", await menuGenerator());
  ejse.data("setting_name", await binder.getSetting("name"));
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/add_letter.ejs`);
}

async function manage_labels_handling() {
  let moduleName = "Electron/manage_labels_handling()";
  ejse.data("programName", programName);
  ejse.data("fileName", binder.getFileName());
  ejse.data("menuHTML", await menuGenerator());
  ejse.data("labelList", await binder.generateLabelList());
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/manage_labels.ejs`);
}

async function viewLetter(uid) {
  let moduleName = "Electron/viewLetter()";
  await binder.getLetterEJS(uid)
  ejse.data("labelList", await binder.generateLabelList());
  mainWindow.loadURL(`file://${__dirname}/assets/ejs/view_letter.ejs`);
}

async function settings_handling(loadPage = true) {
  let moduleName = "Electron/settings_handling()";
  ejse.data("setting_name", await binder.getSetting("name"));
  if (loadPage) mainWindow.loadURL(`file://${__dirname}/assets/ejs/settings.ejs`);
}

// Action handling
async function newBinder(binderPath) {
  let moduleName = "Electron/newBinder()";
  // TODO: Check if a binder is open and if it needs to be saved
  if (isEmpty(binderPath)) {
    binderPath = dialog.showSaveDialogSync({
      title: "Create a new Binder",
      defaultPath: os.homedir(),
      buttonLabel: "Create",
      filters: [{
        name: `${programName} Binders`,
        extensions: ["bndr"]
      }],
      properties: [
        "createDirectory",
        "showOverwriteConfirmation"
      ]
    });
    if (isEmpty(binderPath)) return;
  }
  if (!binderPath.endsWith(".bndr")) binderPath += ".bndr"
  try {
    delete binder
    binder = undefined
    binder = new DataManager(binderPath);
    await binder.create()
    debugHand.log("Created/Loaded binder!", 0, moduleName);
  } catch (e) {
    if (e instanceof DroidsError) electronLogHandling.error(null, e.errMsg, e.moduleName, e.stack);
    else electronLogHandling.error(null, e.message, moduleName, e.stack);
    return;
  }
  if (isEmpty(recentBinders)) {
    try {
      recentBinders = new RecentFilesManager(getConfigFolder() + "/recent_binders.json");
      debugHand.log("Created/Loaded the 'recent binders' file", 1, moduleName);
    } catch (e) {
      if (e instanceof DroidsError) electronLogHandling.error(null, e.errMsg, e.moduleName, e.stack);
      else electronLogHandling.error(null, e.message, moduleName, e.stack);
    }
  }
  recentBinders.addRecent(binder.getFileName(), binder.getFileLocation())
  dashboard_handling()
}

async function loadBinder(binderPath) {
  let moduleName = "Electron/loadBinder()";
  // TODO: Check if a binder is open and if it needs to be saved
  if (isEmpty(binderPath)) {
    binderPath = dialog.showSaveDialogSync({
      title: "Open an existing Binder",
      defaultPath: os.homedir(),
      filters: [{
        name: `${programName} Binders`,
        extensions: ["bndr"]
      }],
      properties: [
        "openFile"
      ]
    });
    if (isEmpty(binderPath)) return;
  }
  if (!binderPath.endsWith(".bndr")) binderPath += ".bndr"
  try {
    delete binder
    binder = undefined
    binder = new DataManager(binderPath);
    await binder.load()
    debugHand.log("Created/Loaded binder!", 0, moduleName);
  } catch (e) {
    if (e instanceof DroidsError && e.errMsg.includes("couldn't be found")) electronLogHandling.error("File not found", "The binder you're trying to open wasn't found.", moduleName, null);
    else if (e instanceof DroidsError) electronLogHandling.error(null, e.errMsg, e.moduleName, e.stack);
    else electronLogHandling.error(null, e.message, moduleName, e.stack);
    return;
  }
  if (isEmpty(recentBinders)) {
    try {
      recentBinders = new RecentFilesManager(getConfigFolder() + "/recent_binders.json");
      debugHand.log("Created/Loaded the 'recent binders' file", 1, moduleName);
    } catch (e) {
      if (e instanceof DroidsError) electronLogHandling.error(null, e.errMsg, e.moduleName, e.stack);
      else electronLogHandling.error(null, e.message, moduleName, e.stack);
    }
  }
  recentBinders.addRecent(binder.getFileName(), binder.getFileLocation())
  letters_overview_handling()
}

// Test Handling
async function test() {
  console.dir(await binder.data.all("SELECT * FROM `settings`"))
}

// Code execution

// ELECTRON: Create main window and some default event handlers
const mainMenuTemplate = [{
    label: 'File',
    submenu: [{
        label: 'New Binder...',
        click(item, focusedWindow) {
          newBinder();
        }
      },
      {
        label: 'Load Binder...',
        click(item, focusedWindow) {
          loadBinder();
        }
      },
      {
        type: 'separator'
      },
      {
        label: 'Exit',
        accelerator: process.platform == "darwin" ? "Command+Q" : "Alt+F4",
        click(item, focusedWindow) {
          mainWindow.close();
        }
      }
    ],
  },
  {
    label: 'About',
    submenu: [
      {
        label: 'About ' + programName + '...',
        click(item, focusedWindow) {
          dialog.showMessageBox(mainWindow, {
            "type": "info",
            "buttons": ["Cool, thanks"],
            "title": "About " + programName,
            "message": `Letter Manager 2020-${new Date().getFullYear()}\n\nLead Developer: Andrew de Jong\n\nYeah nobody else worked on this and I have nothing else to say.\nHave a great day! :D`
          })
        }
      },
      {
        label: "License...",
        click(item, focusedWindow) {
          electron.shell.openExternal("https://gitlab.com/android4682/letter-mananger/-/blob/master/LICENSE")
        }
      }
    ]
  }
];
// If any debug mode is active add developer options to the menu
if (debugHand.getDebugMode() != "NO-DEBUG" && !!debugHand.getDebugMode()) mainMenuTemplate.push({
  label: 'Developer Options',
  submenu: [{
      label: 'Run Test Code',
      click(item, focusedWindow) {
        test()
      }
    },
    {
      type: 'separator'
    },
    {
      role: 'reload'
    },
    {
      label: 'Toggle DevTools...',
      accelerator: process.platform == "darwin" ? "Command+Shift+I" : "Ctrl+Shift+I",
      click(item, focusedWindow) {
        focusedWindow.toggleDevTools();
      }
    },
    {
      label: 'Debug mode...',
      click(item, focusedWindow) {
        dialog.showMessageBox(mainWindow, {
          "type": "info",
          "buttons": ["Okay"],
          "title": "Debug mode",
          "message": `Current debug mode: ${debugHand.getDebugMode()}`
        })
      }
    }
  ]
});

// If Mac, remove empty menu that darwin adds
if (process.platform == 'darwin') mainMenuTemplate.unshift({});
// Start creating the application window
app.on('ready', function () {
  let moduleName = "Electron";
  mainWindow = new electron.BrowserWindow({
    minWidth: 1150,
    minHeight: 700,
    frame: true,
    icon: icon,
    // show: false,
    webPreferences: {
      nodeIntegration: true
    },
    backgroundColor: "#fff"
  });
  ejse.data("version", version)
  const mainMenu = electron.Menu.buildFromTemplate(mainMenuTemplate);

  electron.Menu.setApplicationMenu(mainMenu);

  mainWindow.on("error", function (err) {
    let moduleName = "Electron/MainWindow";
    electronLogHandling.error(null, err.message, moduleName, err.stack);
  });

  mainWindow.on("close", function () {
    app.quit();
  });

  ejse.data("csp", `<meta http-equiv="Content-Security-Policy"
    content="default-src 'self' 'unsafe-inline'; style-src-elem 'self' 'unsafe-inline' https://fonts.googleapis.com; font-src 'self' https://fonts.gstatic.com; img-src 'self' data:; object-src 'self' data:">`);
  index_handling();
});

app.on('error', function (err) {
  let moduleName = "Electron";
  electronLogHandling.error(null, err.message, moduleName, err.stack);
  app.quit();
});

// When a html minimize button is pressed minimize the window
ipcMain.on("minimize", function (event) {
  mainWindow.minimize();
});

// When a html maximize button is pressed maximize the window
ipcMain.on("maximize", function (event) {
  mainWindow.isMaximized() ? mainWindow.unmaximize() : mainWindow.maximize();
});

// When a html close button is pressed start running the close window code
ipcMain.on("close", function (event) {
  mainWindow.close();
});

ipcMain.on("openExternalLink", function (event, href) {
  shell.openExternal(href);
});

ipcMain.on("openPage", function (event, href) {
  let moduleName = "Electron/ipc/openPage";
  href = href.split(".")[0] + "_handling".trim();
  debugHand.log("Received href: ", 3, moduleName, href);
  try {
    eval(href + "()");
  } catch (e) {
    electronLogHandling.error("Tab/Page not found", "Sorry but we couldn't find what you where looking for.\nIf you think this is a bug please report so through our GitLab.", moduleName, e.stack);
  }
});

ipcMain.on("newBinder", function (event, fileLocation) {
  let moduleName = "Electron/ipc/newBinder";
  newBinder(fileLocation);
});

ipcMain.on("loadBinder", function (event, fileLocation) {
  let moduleName = "Electron/ipc/loadBinder";
  // TODO: Maybe show nice error messages if the location couldn't be found anymore?
  loadBinder(fileLocation);
});

ipcMain.on("clearRecents", function (event) {
  let moduleName = "Electron/ipc/clearRecents";
  if (!isEmpty(recentBinders)) {
    recentBinders.setData([]);
    welcomeRecent_handling();
  }
});

ipcMain.on("removeFromRecents", function (event, fileLocation) {
  let moduleName = "Electron/ipc/removeFromRecents";
  if (!isEmpty(recentBinders)) {
    recentBinders.removeRecent("location", fileLocation);
    welcomeRecent_handling();
  }
});

ipcMain.on("addLabel", async function (event, name, color) {
  let moduleName = "Electron/ipc/addLabel";
  if (!isEmpty(binder)) {
    try {
      await binder.addLabel(name, color)
      ejse.data("labelList", await binder.generateLabelList());
      return mainWindow.webContents.send('task-success')
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("deleteLabel", async function (event, labelUID) {
  let moduleName = "Electron/ipc/deleteLabel";
  if (!isEmpty(binder)) {
    try {
      await binder.deleteLabel(labelUID)
      ejse.data("labelList", await binder.generateLabelList());
      return mainWindow.webContents.send('task-success')
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("addLetter", async function (event, from, to, subject, date, labels, fileBase64, type) {
  let moduleName = "Electron/ipc/addLetter";
  if (!isEmpty(binder)) {
    try {
      await binder.addLetter(from, to, subject, date, labels, fileBase64, type)
      return mainWindow.webContents.send('task-success')
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("viewLetter", async function (event, letterUID) {
  let moduleName = "Electron/ipc/viewLetter";
  if (!isEmpty(binder)) viewLetter(letterUID)
  else mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("deleteLetter", async function (event, uid) {
  let moduleName = "Electron/ipc/deleteLetter";
  if (!isEmpty(binder)) {
    try {
      await binder.deleteLetter(uid)
      ejse.data("letterList", await binder.generateLettersList());
      return mainWindow.webContents.send('task-success')
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("editLetter", async (event, uid, field, newValue, cb) => {
  let moduleName = "Electron/ipc/editLetter";
  if (!isEmpty(binder)) {
    try {
      await binder.editLetter(uid, field, newValue)
      if (field == "content") await binder.editLetter(uid, "type", newValue.match(/data:(.+);/)[1])
      return mainWindow.webContents.send(cb, {
        "success": true,
        msg: null
      })
    } catch (e) {
      return mainWindow.webContents.send(cb, {
        "success": false,
        msg: e.message
      })
    }
  }
  return mainWindow.webContents.send(cb, {
    "success": false,
    msg: "No binder is loaded."
  })
})

ipcMain.on("getLetterLabelHTML", async (event, uid, cb) => {
  let moduleName = "Electron/ipc/getLetterLabelHTML";
  if (!isEmpty(binder)) {
    try {
      return mainWindow.webContents.send(cb, {
        "success": true,
        msg: await binder.getLabelHTMLOfLetter(undefined, uid)
      })
    } catch (e) {
      return mainWindow.webContents.send(cb, {
        "success": false,
        msg: e.message
      })
    }
  }
  return mainWindow.webContents.send(cb, {
    "success": false,
    msg: "No binder is loaded."
  })
})

ipcMain.on("getLabelCheckboxes", async (event, letterUID, cb) => {
  let moduleName = "Electron/ipc/getLabelCheckboxes";
  if (!isEmpty(binder)) {
    try {
      return mainWindow.webContents.send(cb, {
        "success": true,
        msg: await binder.generateLabelCheckboxes(letterUID)
      })
    } catch (e) {
      return mainWindow.webContents.send(cb, {
        "success": false,
        msg: e.message
      })
    }
  }
  return mainWindow.webContents.send(cb, {
    "success": false,
    msg: "No binder is loaded."
  })
})

ipcMain.on("editSettings", async function (event, settingObject) {
  let moduleName = "Electron/ipc/editSettings";
  if (!isEmpty(binder)) {
    try {
      settingObject = JSON.parse(settingObject)
      for (const setting in settingObject) await binder.editSetting(setting, settingObject[setting])
      await settings_handling(false)
      return mainWindow.webContents.send('task-success')
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});

ipcMain.on("open-yearbook", async (event, year, args) => {
  let moduleName = "Electron/ipc/open-yearbook";
  if (!isEmpty(binder)) {
    try {
      ejse.data("programName", programName);
      ejse.data("fileName", binder.getFileName());
      ejse.data("menuHTML", await menuGenerator());
      ejse.data("letterList", await binder.generateLettersList(year));
      ejse.data("labelList", await binder.generateLabelList());
      return mainWindow.loadURL(`file://${__dirname}/assets/ejs/letters_overview.ejs`);
    } catch (e) {
      return mainWindow.webContents.send('task-failed', e.message)
    }
  }
  return mainWindow.webContents.send('task-failed', "No binder is loaded.")
});